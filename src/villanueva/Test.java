package villanueva;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Test {

    /**
     * main() handles program execution.
     * @param args takes string of arguments.
     */
    public static void main(String[] args) {
        System.out.println("*****************************************");
        System.out.println("*** This program divides two integers ***");
        System.out.println("*****************************************");

        //Continue execution until this is false
        boolean continueInput = true;

        do {
            //tries to complete the program execution, and if done, changes continueInput to false
            try {
                Scanner input = new Scanner(System.in);
                int a, b;
                System.out.print("Enter the numerator: ");
                a = input.nextInt();

                System.out.print("Enter the denominator: ");
                b = input.nextInt();

                System.out.println("The result is: " + division(a,b));

                continueInput = false;
            } catch (InputMismatchException e) {        //Catching an existing Java exception...
                System.out.println("Ups! It seems that you didn't enter an integer. Please try again...");
            } catch (zeroDenominator e) {               //Catching an exception I wrote
                System.out.println(e.getMessage());
            } finally {                                 //This part is executed no matter what.
                System.out.println("This is the end of the try-catch block.");
            }
        } while (continueInput);

    }

    /**
     * Divides the two numbers.
     * @param num1 integer
     * @param num2 integer. Can't be 0
     * @return num1/num2
     * @throws zeroDenominator throws an instance of zeroDenominator if the denominator == 0.
     */
    public static Integer division(int num1, int num2) throws zeroDenominator{
        if(num2 == 0){
            throw new zeroDenominator("Bad input. Denominator cannot be 0. Please try again...");
        }
        return num1/num2;
    }

}