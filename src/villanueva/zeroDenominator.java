package villanueva;

/**
 * This class creates an instance of zeroDenominator.
 */
public class zeroDenominator extends ArithmeticException {
    public zeroDenominator(String message){
        super(message);
    }
}
